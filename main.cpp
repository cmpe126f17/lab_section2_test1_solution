#include <iostream>
#include "mymatrix.h"

using namespace std;

int main()
{
    srand((unsigned)time(nullptr));
    int M = 3, N = 3;
    //mymatrix matrix1(M, N); //sets a zero matrix
    mymatrix matrix1(3, 3, "allOne");
    mymatrix matrix2(3, 3, "random");
    mymatrix matrix3(4, 5, "allOne");

    cout << "matrix1: " << matrix1 << endl;
    cout << "matrix2: " << matrix2 << endl;

    cout << "matrix1 + matrix2 = " << matrix1 + matrix2 << endl;

    matrix3 = matrix3 + matrix2;
    cout << "matrix3: " << matrix3 << endl;
    return 0;
}
