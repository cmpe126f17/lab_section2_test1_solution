#include <iostream>
#include <string>
#include <stdlib.h>
#include <time.h>

using namespace std;

#ifndef MYMATRIX_H
#define MYMATRIX_H


class mymatrix
{
    public:
        mymatrix(int,int);
        mymatrix(int, int, string);
        mymatrix(const mymatrix& obj);
        ~mymatrix();
        mymatrix operator +(mymatrix rhsMatrix);
        mymatrix operator -(mymatrix rhsMatrix);
        mymatrix& operator =(mymatrix rhsMatrix);
        friend ostream& operator <<(ostream& os, mymatrix rhsMatrix);
    protected:
    private:
        int ** matrix;
        int m,n;

};

#endif // MYMATRIX_H
