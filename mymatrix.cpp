#include "mymatrix.h"

mymatrix::mymatrix(int M, int N)
{
    m=M;//number of rows
    n=N;//number of columns
    if (m==0)
        matrix = nullptr;
    else {
        matrix = new int *[m];
        for (int i = 0; i < m; i++) {
            matrix[i] = new int[n]();//zero initialized
        }
    }
}

mymatrix::mymatrix(int M, int N, string disc)
{
    m=M;//number of rows
    n=N;//number of columns
    if (disc == "random")
    {
        matrix = new int * [m];
        for( int i=0; i<m; i++)
        {
            matrix[i]= new int[n];
            for(int j=0; j<n ; j++)
                matrix[i][j]= rand();
        }
    }
    else if (disc == "allOne")
    {
        matrix = new int * [m];
        for( int i=0; i<m; i++)
        {
            matrix[i]= new int[n];
            for(int j=0; j<n ; j++)
                matrix[i][j]= 1;
        }
    }
    if (m==0)
        matrix = nullptr;
}
mymatrix::mymatrix(const mymatrix& obj)
{
    m=obj.m;
    n=obj.n;
    if (m==0)
        matrix = nullptr;
    else {
        matrix = new int *[m];
        for (int i = 0; i < m; i++) {
            matrix[i] = new int[n]();//zero initialized
            for (int j = 0; j < n; j++) {
                matrix[i][j] = obj.matrix[i][j];
            }
        }
    }

}
mymatrix::~mymatrix()
{
    for(int i = 0 ; i < m ; i++)
    {
        if (matrix[i]!=0)
            delete[] matrix[i];
    }
    if (matrix !=0)
        delete [] matrix;
}

mymatrix mymatrix::operator +(mymatrix rhsMatrix)
{
    if (!(m==rhsMatrix.m && n==rhsMatrix.n))
    {
        cout << "matrices are not the same size, arithmetic incompatible" << endl;
        return mymatrix (0,0);
    }
    else
    {
        mymatrix tmp(m,n);
        for(int i = 0; i<m ; i++)
        {
            for(int j =0; j<n; j++)
            {
                tmp.matrix[i][j]= matrix[i][j]+rhsMatrix.matrix[i][j];
            }
        }
        return tmp;
    }
}

mymatrix mymatrix::operator -(mymatrix rhsMatrix)
{
    if (!(m==rhsMatrix.m && n==rhsMatrix.n))
    {
        mymatrix tmp(0,0);
        cout << "matrices are not the same size, arithmetic incompatible" << endl;
        return tmp;
    }
    else
    {
        mymatrix tmp(m,n);
        for(int i = 0; i<m ; i++)
        {
            for(int j =0; j<n; j++)
            {
                tmp.matrix[i][j]= matrix[i][j]-rhsMatrix.matrix[i][j];
            }
        }
        return tmp;
    }
}

mymatrix & mymatrix::operator =(mymatrix rhsMatrix)
{
    if (!(m==rhsMatrix.m && n==rhsMatrix.n))
    {
        cout << "matrices are not the same size, arithmetic incompatible" << endl;
    }
    else
    {
        for(int i = 0; i<m ; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = rhsMatrix.matrix[i][j];
            }
        }
    }
    return *this;
}
ostream& operator <<(ostream& os, mymatrix rhsMatrix)
{
    if(rhsMatrix.m==0 || rhsMatrix.n==0)
    {
        os << "no matrix to print" << endl;
    }
    else
    {
        for(int i = 0; i<rhsMatrix.m ; i++)
        {
            cout << endl;
            for(int j =0; j<rhsMatrix.n; j++)
            {
                os << rhsMatrix.matrix[i][j] <<"  ";
            }
        }
    }
    return os;
}